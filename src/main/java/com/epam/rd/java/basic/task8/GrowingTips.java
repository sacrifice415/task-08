package com.epam.rd.java.basic.task8;

import java.util.Arrays;
import java.util.List;

public class GrowingTips{
	private static final List<String> LIGHTING_ENUM = Arrays.asList("Yes", "No");
	private String tempreture;
	private String lighting;
	private String watering;
	
	public GrowingTips(String tempreture, String lighting, String watering) {
		super();
		this.tempreture = tempreture;
		this.lighting = validateLighting(lighting);
		this.watering = watering;
	}

	public GrowingTips() {
		
	}

	public String validateLighting(String s) {
		if(LIGHTING_ENUM.contains(s)) {
			return s;
		}
		return "";
	}
	
	public String getTempreture() {
		return tempreture;
	}

	public void setTempreture(String tempreture) {
		this.tempreture = tempreture;
	}

	public String getLighting() {
		return lighting;
	}

	public void setLighting(String lighting) {
		this.lighting = lighting;
	}

	public String getWatering() {
		return watering;
	}

	public void setWatering(String watering) {
		this.watering = watering;
	}



	@Override
	public String toString() {
		return "GrowingTips:\n\t" + "tempreture: " + tempreture + "\n\tlighting: "
				+ lighting + "\n\twatering: " + watering;
	}
	
	
	
}
