package com.epam.rd.java.basic.task8.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.spi.XmlReader;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.GrowingTips;
import com.epam.rd.java.basic.task8.Util;
import com.epam.rd.java.basic.task8.VisualParametrs;
import com.epam.rd.java.basic.task8.constant.XMLflowerConstant;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Flower> flowers;
	Flower flower;
	VisualParametrs visualParametrs;
	GrowingTips growingTips;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		flowers = new ArrayList<>();
	}
	
	
	public List<Flower> getFlowers() {
		
		return flowers;
	}

	public void parseXML(boolean b) {
		XMLInputFactory factory = XMLInputFactory.newInstance()	;
		try {
			XMLEventReader eventReader = factory.createXMLEventReader(new StreamSource(xmlFileName)); 
			XMLEvent xmlEvent;
			while(eventReader.hasNext()) {
				xmlEvent = eventReader.nextEvent();
				if(xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement()	;
					if(startElement.getName().getLocalPart().equals(XMLflowerConstant.FLOWER)) {
						flower = new Flower();
						continue;
					} else if ( startElement.getName().getLocalPart().equals(XMLflowerConstant.FLOWER_NAME)) {
						xmlEvent = eventReader.nextEvent();
						flower.setName(xmlEvent.asCharacters().getData());
						continue;
					} else if ( startElement.getName().getLocalPart().equals(XMLflowerConstant.FLOWER_SOIL)) {
						xmlEvent = eventReader.nextEvent();
						flower.setSoil(xmlEvent.asCharacters().getData());
						continue;
					} else if ( startElement.getName().getLocalPart().equals(XMLflowerConstant.FLOWER_ORIGIN)) {
						xmlEvent = eventReader.nextEvent();
						flower.setOrigin(xmlEvent.asCharacters().getData());
						continue;
					} else if ( startElement.getName().getLocalPart().equals(XMLflowerConstant.VISUAL_PARAMETRS	)) {
						visualParametrs = new VisualParametrs();
						setVisualParameters(eventReader);
						continue;
					} else if ( startElement.getName().getLocalPart().equals(XMLflowerConstant.GROWING_TIPS)) {
						growingTips = new GrowingTips();
						setGrowingTips(eventReader);
						continue;
					} else if ( startElement.getName().getLocalPart().equals(XMLflowerConstant.MULTIPLYING)) {
						xmlEvent = eventReader.nextEvent();
						flower.setMultiplying(xmlEvent.asCharacters().getData());
						continue;
					}
					
				} else if(xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if(endElement.getName().getLocalPart().equals(XMLflowerConstant.FLOWER)) {
						flowers.add(flower);
					} else if(endElement.getName().getLocalPart().equals(XMLflowerConstant.VISUAL_PARAMETRS)) {
						flower.setVisualParameters(visualParametrs);
					} else if(endElement.getName().getLocalPart().equals(XMLflowerConstant.GROWING_TIPS)) {
						flower.setGrowingTips(growingTips);
					}
				}
			}
			System.out.println(getFlowers());
			
			
		} catch(XMLStreamException e) {
			e.printStackTrace();
		}
		
	}

	private void setVisualParameters(XMLEventReader eventReader) {
		XMLEvent event;
		StartElement element;
		Characters character;
		
		for(int i = 0; i<=7; i++) {
			try {
				event = eventReader.nextEvent()	;
				if(event.isStartElement()) {
					element = event.asStartElement();
					if(element.getName().getLocalPart().equals(XMLflowerConstant.VP_STEM_COLOUR)) {
						event = eventReader.nextEvent();
						character = event.asCharacters();
						visualParametrs.setStemColour(character.getData());
					} else if(element.getName().getLocalPart().equals(XMLflowerConstant.VP_LEAF_COLOUR)) {
						event = eventReader.nextEvent();
						character = event.asCharacters();
						visualParametrs.setLeafColour(character.getData());
					} else if(element.getName().getLocalPart().equals(XMLflowerConstant.VP_AVE_LEN_FLOWER)) {
						event = eventReader.nextEvent()	;
						character = event.asCharacters();
						visualParametrs.setAveLenFlower(character.getData());
					}
				}
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
		
	}

	private void setGrowingTips(XMLEventReader eventReader) {
		XMLEvent event;
		StartElement element;
		Characters character;
		
		for(int i = 0; i<=7; i++) {
			try {
				
				event = eventReader.nextEvent()	;
				
				if(event.isStartElement()) {
					
					element = event.asStartElement();
					
					if(element.getName().getLocalPart().equals(XMLflowerConstant.GT_TEMPRETURE)) {
						event = eventReader.nextEvent();
						character = event.asCharacters();
						growingTips.setTempreture(character.getData());
						
					} else if(element.getName().getLocalPart().equals(XMLflowerConstant.GT_LIGHTING)) {
						//event = eventReader.nextEvent();
						growingTips.setLighting(element.getAttributeByName(new QName("lightRequiring")).getValue());
					
					} else if(element.getName().getLocalPart().equals(XMLflowerConstant.GT_WATERING)) {
						event = eventReader.nextEvent()	;
						character = event.asCharacters();
						growingTips.setWatering(character.getData());
					}
				}
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
		
	}




	public void saveToXML(String outputXmlFile) {
		try {
			Util.writeSaxStax(flowers, outputXmlFile);
		} catch (IOException | XMLStreamException e) {
			e.printStackTrace();
		}
		
	}

	// PLACE YOUR CODE HERE

}
