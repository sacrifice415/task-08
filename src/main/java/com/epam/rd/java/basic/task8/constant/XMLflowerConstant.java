package com.epam.rd.java.basic.task8.constant;

public class XMLflowerConstant {
	public static final String FLOWERS = "flowers";
	public static final String FLOWER = "flower";
	public static final String FLOWER_NAME = "name";
	public static final String FLOWER_SOIL = "soil";
	public static final String FLOWER_ORIGIN = "origin";
	public static final String VISUAL_PARAMETRS = "visualParameters";
	public static final String VP_STEM_COLOUR = "stemColour";
	public static final String VP_LEAF_COLOUR = "leafColour";
	public static final String VP_AVE_LEN_FLOWER = "aveLenFlower";
	public static final String GROWING_TIPS = "growingTips";
	public static final String GT_TEMPRETURE = "tempreture";
	public static final String GT_LIGHTING = "lighting";
	public static final String GT_WATERING = "watering";
	public static final String MULTIPLYING = "multiplying";

	// arrays for xml writers
	public static final String[] FLOWER_ARRAY = { FLOWER_NAME, FLOWER_SOIL, FLOWER_ORIGIN, VISUAL_PARAMETRS, GROWING_TIPS,
			MULTIPLYING };
	public static final String[] VISUAL_PARAMETRS_ARRAY = { VP_STEM_COLOUR, VP_LEAF_COLOUR, VP_AVE_LEN_FLOWER };
	public static final String[] GROWING_TIPS_ARRAY = { GT_TEMPRETURE, GT_LIGHTING,
			GT_WATERING };

	private XMLflowerConstant() {
	    throw new IllegalStateException("Utility class XMLTariffConstants");
	  }
}
